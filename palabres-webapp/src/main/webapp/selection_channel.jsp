<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Selection Channel</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    	<link href="bootstrap/style.css" rel="stylesheet">    
    	<s:head />	
	</head>

	<body id="page-top">
		<%@include file="bandeau.jsp" %>
		
		<div class="container" id="corpsdetexte">
			<div class="section">
				<h2>Liste des chans</h2>
				
				<!-- un lien pour rafraichir la liste avec action doList -->
				<s:a action="lister_chan">
					Cliquez pour afficher la liste actualisée des channels
				</s:a>
	    		<ul>
			        <s:iterator value="listChan">
			            <li>

			                <s:a action="go_chan">
			                    <s:property value="name"/>
		                    	<s:param name="name" value="name" />	
		               		</s:a>
			            </li>
			        </s:iterator>
			    </ul>
			</div>
			
			<div class="section">
				<h2>Créer un channel</h2>
				<s:form action="creer_chan" >
					<s:label value="Nom du channel" />
					<s:textfield name="chan.name" requiredLabel="true"/>
					<input class="btn btn-default" type="submit" value="CREER">	
						<s:param name="name">${chan.name}</s:param>
				    </input>
				</s:form>
			</div>
		</div>		
	</body>
</html>