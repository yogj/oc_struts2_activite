<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
	<meta charset="UTF-8">
    <title>Palabres</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
   	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="bootstrap/style.css" rel="stylesheet">    
     <s:head />
</head>

<body>
	<div class="container">
	    <h1>Palabres</h1>

    	<s:form action="authentification" >
     		 <s:label value="Pseudo"/>	
     		 <s:textfield name="utilisateur.pseudo" requiredLabel="true" />

      		<input class="btn btn-default" type="submit" value="S'AUTHENTIFIER">
      			<s:param name="pseudo">${utilisateur.pseudo }</s:param>
     		 </input>

   		 </s:form>
	</div>
	
    
</body>
</html>
