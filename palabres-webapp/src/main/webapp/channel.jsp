
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Channel</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    	<link href="bootstrap/style.css" rel="stylesheet">
    	<s:head/>
	</head>
	
	<body >
		<%@include file="bandeau.jsp" %>
		
		<div class="container" id="corpsdetexte">
			<div id="page-top">
				<h2><s:property value="name"/></h2>
				<table style="margin-left:15px" border="5" cellspacing="1" cellpadding="0" width="100%" class="table table-striped">
					<s:iterator value="listMessage">
						<tr>
							<td><i class="fas fa-user"> <s:property value="author.pseudo"/></i></td>
							<td><s:property value="message"/></td>	
						</tr>
			        </s:iterator>
		        </table>
			</div>	
			
			<div>	
				<s:url action="go_chan" var="refresh">
		           	<s:param name="name" value="name" />	
		        </s:url>
				<a class="btn btn-default" href="${refresh}" role="button">RAFRAICHIR</a>	
			</div>		
		

		</div>

	</body>
</html>