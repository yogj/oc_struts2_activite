package org.example.palabres.action;

import java.util.ArrayList;

import org.example.palabres.model.bean.chat.Channel;
import org.example.palabres.model.bean.chat.Message;
import org.example.palabres.model.bean.utilisateur.Utilisateur;
import org.example.palabres.webapp.WebappHelper;

import com.opensymphony.xwork2.ActionSupport;

public class GoChannelAction extends ActionSupport{
	private Channel chan;
	private ArrayList<Message> listMessage = new ArrayList<Message>();
	private String name;
	private Utilisateur author;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String execute() throws Exception {
        //call Service class to store personBean's state in database
		String vResult = ActionSupport.INPUT;
		//--controle de la nullite du pseudo
    	if (name.length() == 0) {
    			addFieldError("chan.name", "Un nom de channel est requis.");
        		vResult = ActionSupport.INPUT;
        }
    	else {
    		chan = WebappHelper.getManagerFactory().getChatManager().getChannel(name);
    		listMessage = (ArrayList<Message>) WebappHelper.getManagerFactory().getChatManager().getListNewMessage(chan, null);
    		for (Message mess : listMessage) {
    			author = WebappHelper.getManagerFactory().getUtilisateurManager().getUtilisateur(mess.getAuthor().getPseudo());
    			System.out.println(author.getPseudo());
    		}
    		vResult = ActionSupport.SUCCESS;
    	}
    	
		return vResult;

    }

	public Channel getChan() {
		return chan;
	}

	public void setChan(Channel chan) {
		this.chan = chan;
	}

	public ArrayList<Message> getListMessage() {
		return listMessage;
	}

	public void setListMessage(ArrayList<Message> listMessage) {
		this.listMessage = listMessage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
}
