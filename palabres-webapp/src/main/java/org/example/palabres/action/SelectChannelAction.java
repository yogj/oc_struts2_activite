package org.example.palabres.action;

import java.util.ArrayList;
import java.util.Map;

import org.example.palabres.model.bean.chat.Channel;
import org.example.palabres.model.bean.utilisateur.Utilisateur;
import org.example.palabres.model.exception.FunctionalException;
import org.example.palabres.webapp.WebappHelper;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SelectChannelAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Channel chan;
	private ArrayList<Channel> listChan = new ArrayList<Channel>();
	private Map<String, Object> session;
	
	public Channel getChan() {
		return chan;
	}
	public void setChan(Channel chan) {
		this.chan = chan;
	}
	public ArrayList<Channel> getListChan() {
		return listChan;
	}
	public void setListChan(ArrayList<Channel> listChan) {
		this.listChan = listChan;
	}
	public String doListChan() throws Exception {
        //call Service class to store personBean's state in database
		session = ActionContext.getContext().getSession();
		if (((Utilisateur)session.get("utilisateur")).getPseudo().length() != 0)
			listChan = (ArrayList<Channel>) WebappHelper.getManagerFactory().getChatManager().getListChannel();
        
        return SUCCESS;
    }
	
	public String creerChan() throws Exception {
		String vResult = "";
		//--controle de la nullite du nom du chan
    	if (chan.getName().length() == 0) {
    			addFieldError("chan.name", "Un nom est requis.");
        		vResult = ActionSupport.INPUT;
        }
    	else {
    		//--controle du chan ds liste
    		System.out.println(WebappHelper.getManagerFactory().getChatManager().getListChannel().size());
        	for (Channel c : WebappHelper.getManagerFactory().getChatManager().getListChannel()) {
        		if (c.getName().equalsIgnoreCase(chan.getName())) {
        			addFieldError("chan.name", "Le channel existe deja");
            		vResult = ActionSupport.INPUT;
        		}
        	}
    	}
    	
    	//--creation
    	if (vResult != ActionSupport.INPUT) {
    	   	try {
    	   		WebappHelper.getManagerFactory().getChatManager().addChannel(chan);
    	  	} catch (FunctionalException e) {
    			e.printStackTrace();
    		}
    	   	vResult = ActionSupport.SUCCESS;
       }
		
		
		
		return vResult;
		
	}
	

}
