package org.example.palabres.action;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.example.palabres.model.bean.utilisateur.Utilisateur;
import org.example.palabres.model.exception.FunctionalException;
import org.example.palabres.model.exception.NotFoundException;
import org.example.palabres.webapp.WebappHelper;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.ParameterNameAware;



public class Authentification extends ActionSupport implements SessionAware, ServletRequestAware {
    
    private static final long serialVersionUID = 1L;
    
    private Utilisateur utilisateur;
    private String pseudo;
    private Map<String, Object> session;
    private HttpServletRequest servletRequest;
 


    public String getPseudo() {
		return pseudo;
	}


	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}


	public String execute() throws Exception {
        //call Service class to store personBean's state in database
    	WebappHelper.getManagerFactory().getUtilisateurManager().addUtilisateur(utilisateur);
        
        return SUCCESS;
    }
    

    public Utilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

  //
//public void validate(){
//  
//	//--S'assurer qu'un pseudo est saisi
//	if (utilisateur.getPseudo().length() == 0 ||((Utilisateur) this.session.get("utilisateur")).getPseudo().length() == 0 ) {
//         addFieldError("utilisateur.pseudo", "Un pseudo est requis.");
//     }
//	//--Vérifier que le pseudo n'existe pas deja ?
//  
// }
	
	// ==================== Méthodes ====================
    /**
     * Action permettant la connexion d'un utilisateur
     * @return input / success
     */
    public String doLogin() {
    	String vResult = ActionSupport.INPUT;
       	pseudo = utilisateur.getPseudo();
    	System.out.println("pseudo : "+utilisateur.getPseudo());
    	
    ////--controle de la nullite du pseudo
    //if (utilisateur.getPseudo().length() == 0) {
    //		addFieldError("utilisateur.pseudo", "Un pseudo est requis.");
    //		vResult = ActionSupport.INPUT;
    //}
    //else {
    //	//--controle du pseudo ds liste
    //	System.out.println(WebappHelper.getManagerFactory().getUtilisateurManager().getListUtilisateur().size());
    //	for (Utilisateur u : WebappHelper.getManagerFactory().getUtilisateurManager().getListUtilisateur()) {
    //		if (u.getPseudo().equalsIgnoreCase(utilisateur.getPseudo())) {
    //			addFieldError("utilisateur.pseudo", "Le pseudo est deja utilisé");
    //    		vResult = ActionSupport.INPUT;
    //		}
    //	}
    //}
    //	
    //	//--connexion
    //if (vResult != ActionSupport.INPUT) {
    	        	try {
    	        		WebappHelper.getManagerFactory().getUtilisateurManager().addUtilisateur(utilisateur);
    	        	} catch (FunctionalException e) {
    				e.printStackTrace();
    			}
    	        	this.session.put("utilisateur", utilisateur);
    	           
    	        	vResult = ActionSupport.SUCCESS;
    	       //}
    		
    		
    	
    	
    	

        return vResult;
    }



    /**
     * Action de déconnexion d'un utilisateur
     * @return success
     */

    public String doLogout() {

		utilisateur = (Utilisateur) this.session.get("utilisateur");
		if (utilisateur.getPseudo().length() == 0) {
			addFieldError("utilisateur.pseudo", "Un pseudo est requis.");
    		
		}
    	System.out.println(utilisateur.getPseudo());
    	WebappHelper.getManagerFactory().getUtilisateurManager().deleteUtilisateur(utilisateur);
    	this.session.remove("utilisateur");
    	this.servletRequest.getSession().invalidate();
        return ActionSupport.SUCCESS;

    }


	@Override
	public void setSession(Map<String, Object> pSession) {
		// TODO Auto-generated method stub
		this.session = pSession;		
	}


	@Override
	public void setServletRequest(HttpServletRequest pRequest) {
		// TODO Auto-generated method stub
		this.servletRequest = pRequest;
	}


//@Override
//public boolean acceptableParameterName(String parameterName) {
//	// TODO Auto-generated method stub
//	boolean allowedParameterName = true ;
//	System.out.println("TRACE CTRL");
//    if ( parameterName.contains("session")  || parameterName.contains("request") ) {
//        allowedParameterName = false ;
//    } 
//
//    return allowedParameterName;
//}

}
