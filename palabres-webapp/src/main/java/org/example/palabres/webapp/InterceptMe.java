package org.example.palabres.webapp;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.example.palabres.model.bean.utilisateur.Utilisateur;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.ParameterNameAware;

public class InterceptMe implements ParameterNameAware, Interceptor, ServletRequestAware{

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpServletRequest servletRequest;
	private Map<String, Object> session;


	@Override
	public boolean acceptableParameterName(String parameterName) {
		// TODO Auto-generated method stub
		boolean allowedParameterName = true ;
		System.out.println("TRACE CTRL INTERCEPT");
	    if ( parameterName.contains("session")  || parameterName.contains("request") ) {
	        allowedParameterName = false ;
	    } 

	    return allowedParameterName;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		String vResult;
		HttpServletRequest request = ServletActionContext.getRequest();
		String parameterName = "";
		System.out.println((Utilisateur)invocation.getInvocationContext().get("utilisateur"));
		System.out.println((Utilisateur)invocation.getInvocationContext().getSession().get("utilisateur"));
		if(invocation.getInvocationContext().getSession() == null || ((Utilisateur)invocation.getInvocationContext().getSession().get("utilisateur")) == null )//(request.getParameter("utilisateur.pseudo") != null)
			vResult = "error-forbidden";//request.getParameter("utilisateur.pseudo");//--session.utilisateur.pseudo
		else
			parameterName = ((Utilisateur)invocation.getInvocationContext().getSession().get("utilisateur")).getPseudo();
			
		System.out.println(parameterName);

		if (parameterName.equals(null)) {
			vResult = "error-forbidden";
			//addFieldError("utilisateur.pseudo", "Un pseudo est requis.");
    		//vResult = ActionSupport.INPUT;
		}
		else {
		//--controle du pseudo ds liste
		System.out.println(WebappHelper.getManagerFactory().getUtilisateurManager().getListUtilisateur().size());
    	for (Utilisateur u : WebappHelper.getManagerFactory().getUtilisateurManager().getListUtilisateur()) {
    		if (u.getPseudo().equalsIgnoreCase(parameterName)) {
    			vResult = "error-forbidden";
    			//addFieldError("utilisateur.pseudo", "Le pseudo est deja utilisé");
        		//vResult = ActionSupport.INPUT;
    			}
    		}
		}
		
		if (!acceptableParameterName(parameterName))
			vResult = "error-forbidden";
		else
			vResult = invocation.invoke();
		return vResult;
	}

	@Override
	public void setServletRequest(HttpServletRequest pRequest) {
		// TODO Auto-generated method stub
		this.servletRequest = pRequest;
	}
}
